<!DOCTYPE html>
<html>
<head>
	<title>Tabela</title>
	<meta charset="utf-8">
	
</head>
<body>
	<table  border="1">
		<thead>
			<tr>
				<th>Elemento</th>
				<th>Dia da Semana</th>
				<th>Prado do Dia</th>
				<th>Preço</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$diaSemana[0]= "Domingo";
			$diaSemana[1]= "Segunda";
			$diaSemana[2]= "Terça";
			$diaSemana[3]= "Quarta";
			
			$prato[0]="Lasanha";
			$prato[1]="Frango";
			$prato[2]="Arroz";
			$prato[3]="Feijoada";

			$preco[0]="R$ 12,60";
			$preco[1]="R$ 10,00";
			$preco[2]="R$ 09,40	";
			$preco[3]="R$ 11,20";
			
			
			
			foreach($diaSemana as $elemento => $dia_da_semana){
				echo '<tr>';
				echo '<td>'.$elemento.'</td>';
				echo '<td>'.$dia_da_semana.'</td>';
				echo '<td>'.$prato[$elemento].'</td>';
				echo '<td>'.$preco[$elemento].'</td>';
				echo '</tr>';

			}
			?>
		</tbody>
	</table>
	<?php echo "Hoje é ".$diaSemana[date("N")]."o almoço é ".$prato[date("N")]."o preço é ".$preco[date("N")];
	?>
</body>
</html>